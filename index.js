archenemy_command = {
    deck: [],
    ongoing: [],
    active_idx: -1,

    reset: function() {
        // put all cards in index
        this.deck = this.deck.concat(this.ongoing);
        this.ongoing.splice(0, this.ongoing.length);
        this.doom = 0;
        // set active index
        this.active_idx = -1;

        // shuffle using Yates-Fisher
        for (var i = this.deck.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = this.deck[i];
            this.deck[i] = this.deck[j];
            this.deck[j] = temp;
        }
    },

    add: function(name, ongoing) {
        var scheme = {};
        scheme.name = name + '';
        scheme.ongoing = ongoing === true;

        this.deck.push(
            scheme
        );
    },

    set_into_motion: function() {
        // somehting has gone very wrong if this happens
        if (this.deck.length == 0) {
            return;
        }

        if (this.doom > 0) {
            this.doom += 1;
        }


        // advance the index, then get the scheme
        this.active_idx = (this.active_idx + 1) % this.deck.length;
        var current_scheme = this.deck[this.active_idx];

        // if the scheme is ongoing, we remove it from the deck
        if (current_scheme.ongoing) {
            this.ongoing.push(current_scheme);
            this.deck.splice(this.active_idx, 1);
            this.doom = 1;
        }

        return current_scheme;
    },
    doom: 0,
    abandon_ongoing: function(idx) {
        var scheme = this.ongoing.splice(idx, 1)[0];
        this.deck.push(scheme);
    },

    players: [{
            name: 'Boss',
            boss: true,
            life: 60
        },

        {
            name: 'North',
            boss: false,
            life: 20
        },

        {
            name: 'West',
            boss: false,
            life: 20
        },

        {
            name: 'South',
            boss: false,
            life: 20
        },

        {
            name: 'East',
            boss: false,
            life: 20
        },

    ]
};

var pool = [{
    "count": 2,
    "image": "perhaps-youve-met-my-cohort.jpg",
    "name": "perhaps youve met mycohort",
    "ongoing": false
}, {
    "count": 1,
    "image": "your-inescapable-doom.jpg",
    "name": "your inescapable doom",
    "ongoing": true
}, {
    "count": 2,
    "image": "dance-pathetic-marionette.jpg",
    "name": "dance pathetic marionette",
    "ongoing": false
}, {
    "count": 2,
    "image": "behold-the-power-of-destruction.jpg",
    "name": "behold the power of destruction",
    "ongoing": false
}, {
    "count": 1,
    "image": "feed-the-machine.jpg",
    "name": "feed the machine",
    "ongoing": false
}, {
    "count": 2,
    "image": "all-in-good-time.jpg",
    "name": "all in good time",
    "ongoing": false
}, {
    "count": 2,
    "image": "the-fate-of-the-flammable.jpg",
    "name": "the fate of the flammable",
    "ongoing": false
}, {
    "count": 2,
    "image": "embrace-my-diabolical-vision.jpg",
    "name": "embrace my diabolical vision",
    "ongoing": false
}, {
    "count": 1,
    "image": "choose-your-champion.jpg",
    "name": "choose your champion",
    "ongoing": false
}, {
    "count": 1,
    "image": "may-civilization-collapse.jpg",
    "name": "may civilization collapse",
    "ongoing": false
}, {
    "count": 1,
    "image": "my-crushing-masterstroke.jpg",
    "name": "my crushing masterstroke",
    "ongoing": false
}, {
    "count": 1,
    "image": "mortal-flesh-is-weak.jpg",
    "name": "mortal flesh is weak",
    "ongoing": false
}, {
    "count": 2,
    "image": "realms-befitting-my-majesty.jpg",
    "name": "realms befitting my majesty",
    "ongoing": false
}];


chatStream = new Meteor.Stream('chat');
players = new Meteor.Collection("players");

players.insert({
    name: 'Boss',
    boss: true,
    life: 60
});

players.insert({
    name: 'North',
    boss: false,
    life: 20
});

players.insert({
    name: 'West',
    boss: false,
    life: 20
});

players.insert({
    name: 'South',
    boss: false,
    life: 20
});

players.insert({
    name: 'East',
    boss: false,
    life: 20
});


if (Meteor.isClient) {
    sendChat = function(message) {
        if (!('counter' in this)) {
            this.counter = 0
        }
        chatStream.emit('message', message);
        console.log('self: ' + (++counter) + ' ' + message);
    };

    refreshView = function() {
        chatStream.emit('game.state', '');
    }

    reset = function() {
        window.clear();
        chatStream.emit('game.reset', '');
    }

    players = function(e) {
        chatStream.emit('players.update', e);
    }

    chatStream.on('next.scheme', function(message) {
        console.log(message);
        window.update(message);
    });

    chatStream.on('reset', function(message) {
        window.clear()
    });


    Template.header.greeting = function() {
        return "21";
    };
}

if (Meteor.isServer) {
    for (var i = pool.length - 1; i >= 0; i--) {
        for (var k = pool[i].count - 1; k >= 0; k--) {
            var card = pool[i];
            archenemy_command.add(card.image, card.ongoing);
        };
    };

    chatStream.arch = archenemy_command;
    var arch = chatStream.arch;
    arch.reset();

    Meteor.startup(function() {

    });

    chatStream.on('game.reset', function() {
        archenemy_command.reset();
        chatStream.emit('reset', '');
    });


    chatStream.on('players.update', function(e) {
        archenemy_command.players = e.slice(0);
        chatStream.emit('next.scheme', archenemy_command);
    });


    chatStream.on('message', function() {
        archenemy_command.set_into_motion();
        chatStream.emit('next.scheme', archenemy_command);
    })

    chatStream.on('game.state', function() {
        chatStream.emit('next.scheme', archenemy_command);
    })
}