;
(function(window) {
    window.archenemy_command = {
        deck: [],
        ongoing: [],
        active_idx: -1,

        reset: function() {
            // put all cards in index
            this.deck = this.deck.concat(this.ongoing);
            this.ongoing.splice(0, this.ongoing.length);

            // set active index
            this.active_idx = -1;

            // shuffle using Yates-Fisher
            for (var i = this.deck.length - 1; i > 0; i--) {
                var j = Math.floor(Math.random() * (i + 1));
                var temp = this.deck[i];
                this.deck[i] = this.deck[j];
                this.deck[j] = temp;
            }
        },

        add: function(name, ongoing) {
            var scheme = {};
            scheme.name = name + '';
            scheme.ongoing = ongoing === true;

            this.deck.push(
                scheme
            );
        },

        set_into_motion: function() {
            // somehting has gone very wrong if this happens
            if (this.deck.length == 0) {
                return;
            }

            // advance the index, then get the scheme
            this.active_idx = (this.active_idx + 1) % this.deck.length;
            var current_scheme = this.deck[this.active_idx];

            // if the scheme is ongoing, we remove it from the deck
            if (current_scheme.ongoing) {
                this.ongoing.push(current_scheme);
                this.deck.splice(this.active_idx, 1);
            }

            return current_scheme;
        },

        abandon_ongoing: function(idx) {
            var scheme = this.ongoing.splice(idx, 1)[0];
            this.deck.push(scheme);
        }
    };

    var pool = [{
        "count": 2,
        "image": "perhaps-youve-met-my-cohort.jpg",
        "name": "perhaps youve met mycohort",
        "ongoing": false
    }, {
        "count": 1,
        "image": "your-inescapable-doom.jpg",
        "name": "your inescapable doom",
        "ongoing": true
    }, {
        "count": 2,
        "image": "dance-pathetic-marionette.jpg",
        "name": "dance pathetic marionette",
        "ongoing": false
    }, {
        "count": 2,
        "image": "behold-the-power-of-destruction.jpg",
        "name": "behold the power of destruction",
        "ongoing": false
    }, {
        "count": 1,
        "image": "feed-the-machine.jpg",
        "name": "feed the machine",
        "ongoing": false
    }, {
        "count": 2,
        "image": "all-in-good-time.jpg",
        "name": "all in good time",
        "ongoing": false
    }, {
        "count": 2,
        "image": "the-fate-of-the-flammable.jpg",
        "name": "the fate of the flammable",
        "ongoing": false
    }, {
        "count": 2,
        "image": "embrace-my-diabolical-vision.jpg",
        "name": "embrace my diabolical vision",
        "ongoing": false
    }, {
        "count": 1,
        "image": "choose-your-champion.jpg",
        "name": "choose your champion",
        "ongoing": false
    }, {
        "count": 1,
        "image": "may-civilization-collapse.jpg",
        "name": "may civilization collapse",
        "ongoing": false
    }, {
        "count": 1,
        "image": "my-crushing-masterstroke.jpg",
        "name": "my crushing masterstroke",
        "ongoing": false
    }, {
        "count": 1,
        "image": "mortal-flesh-is-weak.jpg",
        "name": "mortal flesh is weak",
        "ongoing": false
    }, {
        "count": 2,
        "image": "realms-befitting-my-majesty.jpg",
        "name": "realms befitting my majesty",
        "ongoing": false
    }];

    for (var i = pool.length - 1; i >= 0; i--) {
        for (var k = pool[i].count - 1; k >= 0; k--) {
            var card = pool[i];
            archenemy_command.add(card.image, card.ongoing);
        };
    };

})(window);