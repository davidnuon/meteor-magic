(function(jQuery) {

    jQuery.each(("input contextmenu invalid").split(" "), function(i, name) {

        jQuery.fn[name] = function(data, fn) {
            if (fn == null) {
                fn = data;
                data = null;
            }

            return arguments.length > 0 ?
                this.bind(name, data, fn) :
                this.trigger(name);
        };
    });

})(jQuery);

var life_manager = {
    players: [],
    render: function() {
        $c = $('#life');
        $c.html('');
        var that = this;
        console.log(this.players);
        for (var i = 0; i < this.players.length; i++) {
            var $item = $('<li></li>');
            var $name = $('<span class="name"></li>');
            var $life = $('<span class="points"></li>');
            var current = this.players[i];
            if (current.boss) {
                $item.addClass('boss');
            };

            $item.data('current', current);
            $item.data('index', i);

            $name.html(current.name);
            $name.attr('contenteditable', true);
            $name.bind('input', function(e) {
                var data = $(this).parent().data('current');
                var idx = $(this).parent().data('index');
                data.name = $(this).html();
                that.players[idx] = data;
                $(this).parent().data('current', data);
                players(that.players);
            });

            $life.html(current.life);
            $life.attr('contenteditable', true);
            $life.bind('input', function(e) {
                var data = $(this).parent().data('current');
                var idx = $(this).parent().data('index');
                data.life = $(this).html();
                that.players[idx] = data;
                $(this).parent().data('current', data);
                players(that.players);
            });

            $item.append($name);
            $item.append($life);

            $c.append($item);
            console.log(current);
        };
    }
};

$(window).ready(function(argument) {
    dm = false || (location.hash == '#dm');
});

$(function() {
    document.onkeyup = checkKey;
    var $current = $('#current');
    var $active = $('#active');
    var $count = $('#count');
    var count = 0;
    $current.hide();
    $active.hide();

    function checkKey(e) {

        e = e || window.event;

        if (e.keyCode == 13) {
            sendChat();
        }
    }

    function clear() {
        $current.hide();
        $active.hide();
    }

    function update(arch) {
        if (arch.active_idx > -1) {
            if (arch.doom > 0) {
                $count.html(arch.doom);
            }

            card = arch.deck[arch.active_idx];
            if (arch.doom != 1) {
                $active.find('img').attr('src', './img/' + card.name);
                $active.show();
            } else {
                $active.hide();
            }

            if (arch.ongoing.length > 0) {
                $current.find('img').attr('src', './img/' + arch.ongoing[0].name);
                $current.show();
            }
        }

        if (life_manager.players.length == 0 || !dm) {
            life_manager.players = arch.players;
            life_manager.render();
        }

    }

    window.update = update;
    window.clear = clear;

    refreshView();

    $('.points').attr('contenteditable', 'true');
});